## code to prepare `data_squirrels` dataset goes here

data_act_squirrels <- utils::read.csv2("~/data/nyc_squirrels_act_sample.csv",
                                       sep = ",")
data_act_squirrels <- data_act_squirrels[1:15,]

usethis::use_data(data_act_squirrels, overwrite = TRUE)
