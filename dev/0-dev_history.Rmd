---
title: "Development actions history"
output: html_document
editor_options: 
  chunk_output_type: console
---

All commands that you use to use when developing packages...

# First time just after creating the project

- Fill the following chunk to create the DESCRIPTION of your package

```{r description, eval=FALSE}
# Describe your package
fusen::fill_description(
  pkg = here::here(),
  fields = list(
    Title = "Traiter un jeu de donnees sur les ecureuils new-yorkais",
    Description = "Ce package permet d'exploiter un jeu de donnees sur les ecureils de Central Park.",
    `Authors@R` = c(
      person("Blandine", "Legendre", email = "blandine.legendre@insee.fr", role = c("aut", "cre")),
      person(given = "ThinkR", role = "cph")
    )
  )
)
# Define License with use_*_license()
usethis::use_mit_license("Blandine Legendre")
```

# Start using git

```{r, eval=FALSE}
usethis::use_git()
# Link with remote
usethis::use_git_remote(
  "origin",
  url = "https://gitlab.com/octobre_n2_grpa/squirrelsblandine.git",
  overwrite = TRUE)
# Deal with classical files to ignore
usethis::git_vaccinate()
# Use main for primary branch
usethis::git_default_branch_rename()
```

# Set extra sources of documentation

```{r, eval=FALSE}
# Install a first time
remotes::install_local()
# README
usethis::use_readme_rmd()
# Code of Conduct
usethis::use_code_of_conduct("contact@fake.com")
# NEWS
usethis::use_news_md()
# Coverage check
covr::report()
```

**From now, you will need to "inflate" your package at least once to be able to use the following commands. Let's go to your flat template, and come back here later if/when needed.**


# Package development tools
## Use once

```{r, eval=FALSE}
# Pipe
usethis::use_pipe()

# package-level documentation
usethis::use_package_doc()

# GitHub
# Add your credentials for GitHub
gitcreds::gitcreds_set()
# Send your project to a new GitHub project
usethis::use_github()

# Set Continuous Integration
# _GitHub
usethis::use_github_action_check_standard()
usethis::use_github_action("pkgdown")
usethis::use_github_action("test-coverage")
# _GitLab
gitlabr::use_gitlab_ci(type = "check-coverage-pkgdown")

# Add new flat template
fusen::add_flat_template("add")
```

## Use everytime needed

```{r}
# Simulate package installation
pkgload::load_all()

# Generate documentation and deal with dependencies
attachment::att_amend_desc()

# Check the package
devtools::check()
```

# Share the package

```{r}
# set and try pkgdown documentation website
usethis::use_pkgdown()
pkgdown::build_site(override = list(destination = "inst/site"))

# build the tar.gz with vignettes to share with others
devtools::build(vignettes = TRUE)
```

```{r}
# creer un jeu de donnees embarque et sa documentation
usethis::use_data_raw(name = "data_act_squirrels")

# install.packages('checkhelper', repos = 'https://thinkr-open.r-universe.dev')
checkhelper::use_data_doc(name = "data_act_squirrels")
checkhelper::print_globals(quiet = TRUE)

attachment::att_amend_desc()

```


```{r}
# pipe
usethis::use_pipe()
attachment::att_amend_desc()
```

