---
title: "Check data"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# check_primary_color_is_ok
    
```{r function-check_primary_color_is_ok}
#' Verifier si la couleur choisie est correcte
#' 
#' Verifie si la couleur choisie fait partie de la liste des couleurs possibles pour un ecureuil de Central Park : Black, Cinnamon ou Gray
#' 
#' @param string character string
#' 
#' @return Boolean 
#' 
#' @export
check_primary_color_is_ok <- function(string) {
  
  
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  
  if (isFALSE(all_colors_OK)) {
    stop("Toutes les couleurs ne sont pas ok")
  }
  
  return(all_colors_OK)
}
```
  
```{r example-check_primary_color_is_ok}
check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
check_primary_color_is_ok(string = c("Gray", "Cinnamon"))
# check_primary_color_is_ok(string = c("Gray", "Blue"))
```
  
```{r tests-check_primary_color_is_ok}
test_that("check_primary_color_is_ok works", {
  expect_true(inherits(check_primary_color_is_ok, "function"))
  
  expect_true(
    object = check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
  )

  expect_true(
    object = check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black"))
  )
  
  expect_error(
    object = check_primary_color_is_ok(string = c("Gray", "Blue")),
    regexp = "Toutes les couleurs ne sont pas ok"
  )
})
```

# check_squirrel_data_integrity
    
```{r function-check_squirrel_data_integrity}
#' Verifier l'integriter du jeu de donnees squirrels
#' 
#' Blabla
#' 
#' @return rien
#' 
#' @param dataset jeu de donnees
#' 
#' @importFrom utils read.csv2
#' 
#' @export
check_squirrel_data_integrity <- function(dataset){
  
    if("primary_fur_color" %in% colnames(dataset)){
      
      check_primary_color_is_ok(dataset$primary_fur_color)
      
      message("Tout est ok")
      
    } else {
      
      stop("Le jeu de donnees ne contient pas la colonne primary_fur_color.")
      
    }
    
}
```
  
```{r example-check_squirrel_data_integrity}
chem_data <- system.file("nyc_squirrels_sample.csv", package = "squirrelsblandine")
    
    data_squirrels <- read.csv2(file = chem_data, 
                      sep = ",")
    
check_squirrel_data_integrity(data_squirrels)
```
  
```{r tests-check_squirrel_data_integrity}
test_that("check_squirrel_data_integrity works", {
  expect_true(inherits(check_squirrel_data_integrity, "function")) 
  
  chem_data <- system.file("nyc_squirrels_sample.csv", package = "squirrelsblandine")
    
    data_squirrels <- read.csv2(file = chem_data, 
                      sep = ",")
  
  
  expect_message(object =  check_squirrel_data_integrity(data_squirrels),
              regexp = "Tout est ok")
  
  data_squirrels_err <- data_squirrels
 data_squirrels_err[1, "primary_fur_color"] <- "rose"
  
  expect_error(object =  check_squirrel_data_integrity(data_squirrels_err),
              regexp = "Toutes les couleurs ne sont pas ok")
  
})
```
  
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_check_data.Rmd", vignette_name = "Check data")
```

